package com.stvayush.myapplication.module.repository.auth

import androidx.lifecycle.LiveData
import com.stvayush.myapplication.module.api.auth.AuthApi
import com.stvayush.myapplication.module.api.auth.network_repsonses.LoginResponse
import com.stvayush.myapplication.module.api.auth.network_repsonses.RegisterResponse
import com.stvayush.myapplication.module.persistence.AccountPropsDao
import com.stvayush.myapplication.module.persistence.AuthTokenDao
import com.stvayush.myapplication.module.session.SessionManager
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse

/** The @AuthRepository will hold everything that is related to Authentication like loggingIn, changingPassword, signingUp
 *  that's why it takes almost everything related to auth in its constructor
 *  Also note that the repository has no idea of the ViewModel while in ViewModel, we have an instance of it
 *  But Repository has reference to local_database, networks and SessionManager
 *  I'll be using LiveData to observe the ViewModels from repos
 *  */

class AuthRepository constructor(
  val accountPropsDao: AccountPropsDao,
  val authApi: AuthApi,
  val sessionManager: SessionManager,
  val authTokenDao: AuthTokenDao
) {

  fun testLogin(
    email: String,
    password: String
  ): LiveData<GenericApiResponse<LoginResponse>> {
    return authApi.login(email, password)
  }

  fun testRegister(
    email: String,
    username: String,
    password: String,
    confirmPassword: String
  ): LiveData<GenericApiResponse<RegisterResponse>> {
    return authApi.register(email, username, password, confirmPassword)
  }

}