package com.stvayush.myapplication.module.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** This will be a table for managing the authentication token */

/** Here, I'll be setting up a foreignKey relationship between AccountProps_table and this table,
 *  i.e., the foreign of this table will be pointing(referencing) to the foreignKey of the AccountProps_table
 *  Consider this foreignKey relationship as the following instance:
 *  Treat the AccountProps_table as parent and this table as its child and
 *  a child always bears/has(I don't know the appropriate word) a reference to its parent
 *  **/

@Entity(
  tableName = "auth_token_table",
  foreignKeys = [ForeignKey(                 // Initialization for foreignKey
    entity = AccountPropsEntity::class,      // Mapping the Parent table Class
    childColumns = ["account_pk"],           // Telling Room the childColumn(the one which is supposed to match its key), the matchee
    parentColumns = ["pk"],                  // Telling room the parent column(the one to match the child's key with), the matcher
    onDelete = CASCADE                       // It means, if for some reason the pk row(in AccPropsEntity) gets deleted/removed from database,
  )]                                         // then all the foreignKey relationships will also be deleted
)

data class AuthTokenEntity(
  /* These fields are set nullable as the user may or may not be authenticated and we'll be getting token for the authenticated user*/

  //Note that it doesn't posses retrofit annotations as we won't be getting it from server, we'll be using it locally(in local database)

  @PrimaryKey
  @ColumnInfo(name = "account_pk")       //This is the foreignKey that is going to point to accountProps_table
  var account_pk: Int? = -1,    //It is initialized xero to show that, initially, there is no foreignKey relationship established, we'll have to establish it

  @SerializedName("token")
  @Expose
  @ColumnInfo(name = "token")
  var token: String? = null
)