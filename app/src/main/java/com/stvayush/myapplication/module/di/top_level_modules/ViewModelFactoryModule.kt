package com.stvayush.myapplication.module.di.top_level_modules

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module

/** Module for generating ViewModels */

@Module
abstract class ViewModelFactoryModule {

  @Binds
  abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}
