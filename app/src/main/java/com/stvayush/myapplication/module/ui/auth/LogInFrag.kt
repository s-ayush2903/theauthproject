package com.stvayush.myapplication.module.ui.auth

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.stvayush.myapplication.R
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse.ApiEmptyResponse
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse.ApiErrorResponse
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse.ApiSuccessResponse

class LogInFrag : BaseAuthFragment() {

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_log_in, container, false)
  }

  override fun onViewCreated(
    view: View,
    savedInstanceState: Bundle?
  ) {
    super.onViewCreated(view, savedInstanceState)

    authViewModel.testLogin()
      .observe(viewLifecycleOwner, Observer { response ->
        when (response) {
          is ApiSuccessResponse -> {
            Log.d(TAG, "Login Response: ${response.body}")
          }

          is ApiErrorResponse -> {
            Log.d(TAG, "Login Response: ${response.errorMessage}")
          }

          is ApiEmptyResponse -> {
            Log.d(TAG, "Login Response: Empty Body")
          }

        }
      })

  }
}