package com.stvayush.myapplication.module.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** A Room Persistence Entity class
 * This will also be retrofit response class for accountProps
 * */

/** This will be a local-table in the SQLite database using Room persistence library */
@Entity(tableName = "account_props")
data class AccountPropsEntity(

  @SerializedName("pk")         //pk is the PrimaryKey. Its auto-generation is set to false as we'll be getting this from the server
  @Expose
  @PrimaryKey(autoGenerate = false)
  @ColumnInfo(name = "pk")
  var pk: Int,

  @SerializedName("email")
  @Expose
  @ColumnInfo(name = "email")
  var email: String,

  @SerializedName("username")
  @Expose
  @ColumnInfo(name = "username")
  var username: String

)