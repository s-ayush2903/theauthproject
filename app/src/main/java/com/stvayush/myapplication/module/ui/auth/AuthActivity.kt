package com.stvayush.myapplication.module.ui.auth

import android.os.Bundle
import com.stvayush.myapplication.R
import com.stvayush.myapplication.module.top_level_bases.BaseActivity

class AuthActivity : BaseActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_auth)
  }
}
