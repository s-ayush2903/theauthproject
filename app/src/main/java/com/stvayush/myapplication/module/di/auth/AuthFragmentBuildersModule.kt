package com.stvayush.myapplication.module.di.auth

import com.stvayush.myapplication.module.ui.auth.ForgotPasswordFrag
import com.stvayush.myapplication.module.ui.auth.LauncherFrag
import com.stvayush.myapplication.module.ui.auth.LogInFrag
import com.stvayush.myapplication.module.ui.auth.RegisterFrag
import dagger.Module
import dagger.android.ContributesAndroidInjector

/** For Providing Fragments as dependencies for auth section of app */

@Module
abstract class AuthFragmentBuildersModule {

  @ContributesAndroidInjector()
  abstract fun contributeLauncherFragment(): LauncherFrag

  @ContributesAndroidInjector()
  abstract fun contributeLoginFragment(): LogInFrag

  @ContributesAndroidInjector()
  abstract fun contributeRegisterFragment(): RegisterFrag

  @ContributesAndroidInjector()
  abstract fun contributeForgotPasswordFragment(): ForgotPasswordFrag

}