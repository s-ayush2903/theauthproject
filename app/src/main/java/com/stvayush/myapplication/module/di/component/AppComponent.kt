package com.stvayush.myapplication.module.di.component

import android.app.Application
import com.stvayush.myapplication.module.top_level_bases.BaseApplication
import com.stvayush.myapplication.module.di.top_level_modules.ActivityBuildersModule
import com.stvayush.myapplication.module.di.top_level_modules.AppModule
import com.stvayush.myapplication.module.di.top_level_modules.ViewModelFactoryModule
import com.stvayush.myapplication.module.session.SessionManager
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBuildersModule::class,
    ViewModelFactoryModule::class
  ]
)
interface AppComponent : AndroidInjector<BaseApplication> {

  val sessionManager: SessionManager // must add here b/c injecting into abstract class

  @Component.Builder
  interface Builder {

    @BindsInstance
    fun application(application: Application): Builder

    fun build(): AppComponent
  }
}