package com.stvayush.myapplication.module.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.stvayush.myapplication.R
import com.stvayush.myapplication.databinding.FragmentLauncherBinding
import kotlinx.android.synthetic.main.fragment_launcher.focusable_view

/**
 * A simple [Fragment] subclass.
 */
class LauncherFrag : BaseAuthFragment() {
  private lateinit var binding: FragmentLauncherBinding
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_launcher, container, false)
    return binding.root
  }

  override fun onViewCreated(
    view: View,
    savedInstanceState: Bundle?
  ) {
    super.onViewCreated(view, savedInstanceState)
    binding.apply {
      register.setOnClickListener {
        findNavController().navigate(
          R.id.action_launcherFrag_to_registerFrag
        )
      }
      login.setOnClickListener {
        findNavController().navigate(
          R.id.action_launcherFrag_to_logInFrag
        )
      }
      forgotPassword.setOnClickListener {
        findNavController().navigate(
          R.id.action_launcherFrag_to_forgotPasswordFrag
        )
      }
    }
    focusable_view.requestFocus()
  }
}