package com.stvayush.myapplication.module.session

import android.app.Application
import com.stvayush.myapplication.module.persistence.AuthTokenDao
import javax.inject.Inject
import javax.inject.Singleton

/** Session Manager takes care of the unique user-token throughout the session whether the user has app on or off or in background .
 *  Its function is to store the token in the sharedPrefs and delete it when the app goes background
 *  It will be emitting data to repo using Observables
 **/
@Singleton
class SessionManager @Inject constructor(
  val authTokenDao: AuthTokenDao,
  val application: Application
) {

}