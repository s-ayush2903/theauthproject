package com.stvayush.myapplication.module.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.stvayush.myapplication.module.entities.AccountPropsEntity

/** Dao for account properties
 *  DAOs are the interfaces that are used to access the local database class(abstract) for retrieving tables and data in it
 *  This DAO will contain all user's account related info. like its credentials
 *  Here methods will be written to interact with the database
 * */

@Dao
interface AccountPropsDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertAndReplace(accountPropsEntity: AccountPropsEntity): Long

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  fun insertOrIgnore(accountPropsEntity: AccountPropsEntity): Long

  /** The following SQL statement means:
   * Select everything from account_props named table where pk is the
   * value that I'm passing in through the following function as input */
  @Query("SELECT * FROM account_props WHERE pk= :pk")          // This is a raw SQL statement interpreted by Room and executed on the database
  fun searchByPk(pk:Int): LiveData<AccountPropsEntity>               // Colon(:) annotation is added to pk as I'll be passing value in it

  @Query("SELECT * FROM account_props WHERE email= :email")
  fun searchByEmail(email:String): AccountPropsEntity?

}