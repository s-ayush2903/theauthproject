package com.stvayush.myapplication.module.di.top_level_modules

import com.stvayush.myapplication.module.di.auth.AuthFragmentBuildersModule
import com.stvayush.myapplication.module.di.auth.AuthModule
import com.stvayush.myapplication.module.di.auth.AuthScope
import com.stvayush.myapplication.module.di.auth.AuthViewModelModule
import com.stvayush.myapplication.module.ui.auth.AuthActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

  @AuthScope
  @ContributesAndroidInjector(
    modules = [AuthModule::class, AuthFragmentBuildersModule::class, AuthViewModelModule::class]
  )
  abstract fun contributeAuthActivity(): AuthActivity

}