package com.stvayush.myapplication.module.api.auth.network_repsonses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse(

  @SerializedName("reponse")
  @Expose
  var response: String,

  @SerializedName("error_message")
  @Expose
  var errorMessage: String,

  @SerializedName("pk")
  @Expose
  var pk: Int,

  @SerializedName("email")
  @Expose
  var email: String,

  @SerializedName("token")
  @Expose
  var token: String

) {
  override fun toString(): String {
    return "LoginResponse(response='$response', errorMessage='$errorMessage', token='$token', pk=$pk, email='$email')"
  }
}