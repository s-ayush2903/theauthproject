package com.stvayush.myapplication.module.di.auth

import androidx.lifecycle.ViewModel
import com.stvayush.myapplication.module.di.scopes.ViewModelKey
import com.stvayush.myapplication.module.ui.auth.AuthViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelModule {

  @Binds
  @IntoMap
  @ViewModelKey(AuthViewModel::class)
  abstract fun bindAuthViewModel(authViewModel: AuthViewModel): ViewModel

}