package com.stvayush.myapplication.module.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.stvayush.myapplication.module.entities.AuthTokenEntity

/** The AuthToken table is for authentication only, it has nothing to do with account props.
 * This will contain AuthToken_table for storing authentication tokens
 * The table will be storing token only when the user is authenticated
 * */

@Dao
interface AuthTokenDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(authTokenEntity: AuthTokenEntity): Long

  /* This will be used when user has logs out of the app
  * , as result of which the token will be set null in the database
   */
  @Query("UPDATE auth_token_table SET token= null WHERE account_pk=:pk")
  fun nullifyToken(pk: Int): Int
}