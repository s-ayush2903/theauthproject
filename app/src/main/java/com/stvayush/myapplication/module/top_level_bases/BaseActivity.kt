package com.stvayush.myapplication.module.top_level_bases

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

  val TAG: String = "App Debug"

}