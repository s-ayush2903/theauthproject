package com.stvayush.myapplication.module.api.auth.network_repsonses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterResponse(

  @SerializedName("error_message")
  @Expose
  var errorMessage: String,

  @SerializedName("response")
  @Expose
  var response: String,

  @SerializedName("email")
  @Expose
  var email: String,

  @SerializedName("username")
  @Expose
  var username: String,

  @SerializedName("pk")
  @Expose
  var pk: Int,

  @SerializedName("token")
  @Expose
  var token: String

) {
  override fun toString(): String {
    return "RegistrationResponse(errorMessage='$errorMessage'response ='$response', email ='$email', username ='$username', pk ='$pk', token ='$token')"
  }
}