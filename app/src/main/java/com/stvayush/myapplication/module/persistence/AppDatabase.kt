package com.stvayush.myapplication.module.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.stvayush.myapplication.module.entities.AccountPropsEntity
import com.stvayush.myapplication.module.entities.AuthTokenEntity

/** As the name suggests it is the database for the app
 *  It generates SQLite database and the created DAOs(DAO classes) access this database class and tables inside it
 * */

@Database(entities = [AccountPropsEntity::class, AuthTokenEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

  abstract fun getAuthTokenDao(): AuthTokenDao

  abstract fun getAccountPropsDao(): AccountPropsDao

  companion object {

    const val DATABASE_NAME = "app_database"

  }

}