package com.stvayush.myapplication.module.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.stvayush.myapplication.module.api.auth.network_repsonses.LoginResponse
import com.stvayush.myapplication.module.api.auth.network_repsonses.RegisterResponse
import com.stvayush.myapplication.module.repository.auth.AuthRepository
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse
import javax.inject.Inject

/** Note that the ViewModel has a reference to the Repository whereas in the repository, there's nothing like that*/

class AuthViewModel @Inject constructor(val authRepository: AuthRepository) : ViewModel() {

  fun testLogin(): LiveData<GenericApiResponse<LoginResponse>> {
    return authRepository.testLogin("friend@gmail.com", "be_happy")
  }

  fun testRegister(): LiveData<GenericApiResponse<RegisterResponse>> {
    return authRepository.testRegister(
      "ayush_1901cb14@iitp.ac.in", "s-ayush2903", "password", "password"
    )
  }

}