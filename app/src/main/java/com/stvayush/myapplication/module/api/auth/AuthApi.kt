package com.stvayush.myapplication.module.api.auth

import androidx.lifecycle.LiveData
import com.stvayush.myapplication.module.api.auth.network_repsonses.LoginResponse
import com.stvayush.myapplication.module.api.auth.network_repsonses.RegisterResponse
import com.stvayush.myapplication.module.utils.network_utils.GenericApiResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/** Make Networking calls related to Auth here */

interface AuthApi {

  @POST("account/login")
  @FormUrlEncoded
  fun login(
    @Field("username") email: String,
    @Field("password") password: String
  ): LiveData<GenericApiResponse<LoginResponse>>

  @POST("account/register")
  @FormUrlEncoded
  fun register(
    @Field("email") email: String,
    @Field("username") username: String,
    @Field("password") password: String,
    @Field("password2") password2: String
  ):LiveData<GenericApiResponse<RegisterResponse>>

}