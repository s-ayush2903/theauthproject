package com.stvayush.myapplication.module.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.stvayush.myapplication.R

/**
 * A simple [Fragment] subclass.
 */
class ForgotPasswordFrag : BaseAuthFragment() {

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_forgot_password, container, false)
  }

}
