package com.stvayush.myapplication.module.di.top_level_modules

import android.app.Application
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.stvayush.myapplication.R
import com.stvayush.myapplication.module.persistence.AccountPropsDao
import com.stvayush.myapplication.module.persistence.AppDatabase
import com.stvayush.myapplication.module.persistence.AppDatabase.Companion.DATABASE_NAME
import com.stvayush.myapplication.module.persistence.AuthTokenDao
import com.stvayush.myapplication.module.utils.basic_utils.Constants
import com.stvayush.myapplication.module.utils.network_utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/** This module contains the dependencies that exist in app throughout its lifetime */

@Module
class AppModule {

  @Singleton
  @Provides
  fun provideGsonBuilder(): Gson {                               // @excludeFieldsWithoutExposeAnnotation Says that for any field that doesn't has @Expose annotation
    return GsonBuilder().excludeFieldsWithoutExposeAnnotation() // we don't want Retrofit to look for it
      .create()
  }

  @Singleton
  @Provides
  fun providesRetrofit(gson: Gson):Retrofit.Builder{
    return Retrofit.Builder()
      .baseUrl(Constants.BASE_URL)
      .addCallAdapterFactory(
        LiveDataCallAdapterFactory()
      )
      .addConverterFactory(GsonConverterFactory.create(gson))
  }


  @Singleton
  @Provides
  fun provideAppDb(app: Application): AppDatabase {
    return Room
      .databaseBuilder(app, AppDatabase::class.java, DATABASE_NAME)
      .fallbackToDestructiveMigration() // get correct db version if schema changed
      .build()
  }

  @Singleton
  @Provides
  fun provideAuthTokenDao(db: AppDatabase): AuthTokenDao {
    return db.getAuthTokenDao()
  }

  @Singleton
  @Provides
  fun provideAccountPropertiesDao(db: AppDatabase): AccountPropsDao {
    return db.getAccountPropsDao()
  }

  @Singleton
  @Provides
  fun provideRequestOptions(): RequestOptions {
    return RequestOptions
      .placeholderOf(R.drawable.red_button_drawable)
      .error(R.drawable.red_button_drawable)
  }

  @Singleton
  @Provides
  fun provideGlideInstance(
    application: Application,
    requestOptions: RequestOptions
  ): RequestManager {
    return Glide.with(application)
      .setDefaultRequestOptions(requestOptions)
  }

}