package com.stvayush.myapplication.module.utils.network_utils

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** It looks for parameter 'response' in the responses received from server and matches it to @response var */

class GenericResponse(

    @SerializedName("repsonse")
    @Expose
    var response: String

)