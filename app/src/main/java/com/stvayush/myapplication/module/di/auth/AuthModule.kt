package com.stvayush.myapplication.module.di.auth

import com.stvayush.myapplication.module.api.auth.AuthApi
import com.stvayush.myapplication.module.persistence.AccountPropsDao
import com.stvayush.myapplication.module.persistence.AuthTokenDao
import com.stvayush.myapplication.module.repository.auth.AuthRepository
import com.stvayush.myapplication.module.session.SessionManager
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class AuthModule {

  @AuthScope
  @Provides
  fun provideFakeApiService(retrofitBuilder: Retrofit.Builder): AuthApi {
    return retrofitBuilder
      .build()
      .create(AuthApi::class.java)
  }

  @AuthScope
  @Provides
  fun provideAuthRepository(
    sessionManager: SessionManager,
    authTokenDao: AuthTokenDao,
    accountPropertiesDao: AccountPropsDao,
    authApi: AuthApi
  ): AuthRepository {
    return AuthRepository(
      accountPropertiesDao,
      authApi,
      sessionManager,
      authTokenDao

    )
  }

}